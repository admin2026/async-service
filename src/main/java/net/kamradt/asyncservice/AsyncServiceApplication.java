package net.kamradt.asyncservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import reactor.core.publisher.Mono;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
@Slf4j
public class AsyncServiceApplication {
	public BlockingQueue<JsonNode> queue = new ArrayBlockingQueue<>(4);
	public ObjectMapper objectMapper = new ObjectMapper();
	@Autowired
	public ReactiveElasticsearchOperations operations;


	public static void main(String[] args) {
		SpringApplication.run(AsyncServiceApplication.class, args);
	}

	@Bean
	public Supplier<String> eventProducer(){
		return () -> {
			try {
				JsonNode item = queue.take();
				log.info("pulled {} from queue", item);
				return item.toPrettyString();
			} catch (InterruptedException e) {
				log.warn("process interrupted");
				throw new IllegalStateException("process interrupted");

			}
		};
	}

	@Bean
	public Consumer<String> eventConsumer() {
		return value -> Mono.just(value)
				.flatMap(v -> {
					try {
						return Mono.just(objectMapper.readTree(v));
					} catch (JsonProcessingException e) {
						log.warn("exception deserializing {}", v);
						return Mono.error(e);
					}
				})
				.doOnNext(json -> log.info("message {} received", json))
				.flatMap(json -> operations.save(json)) //Mono.just(json)))
				.map(response -> queue.add(response))
				.subscribe(i -> log.info("subscribe returned {}", i),
						e -> log.warn("subscribe returned error", e));
	}

}
