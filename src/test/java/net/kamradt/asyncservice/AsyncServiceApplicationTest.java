package net.kamradt.asyncservice;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.*;

/** 
* AsyncServiceApplication Tester. 
* 
* @author <Authors name> 
* @since <pre>Dec 25, 2022</pre> 
* @version 1.0 
*/
@RunWith(MockitoJUnitRunner.class)
public class AsyncServiceApplicationTest {
    @Mock
    ReactiveElasticsearchOperations operations;
    /**
     * Method: main(String[] args)
     */
    @Test
    public void testProcess() throws Exception {
        AsyncServiceApplication sut = new AsyncServiceApplication();
        Mockito.when(operations.save(any(JsonNode.class))).thenReturn(Mono.just(
                sut.objectMapper.createObjectNode().put("message", "success")));
        sut.operations = operations;

        sut.eventConsumer().accept("{ \"message\": \"this is the message\" }");
        String value = sut.eventProducer().get();
        JsonNode json = sut.objectMapper.readTree(value);
        Assert.assertTrue(json.hasNonNull("message"));
        Assert.assertEquals("success", json.path("message").asText(""));
    }
}